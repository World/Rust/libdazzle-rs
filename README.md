# libdazzle-rs

The Rust bindings of [libdazzle](https://gitlab.gnome.org/GNOME/libdazzle)

**Note**: libdazzle-rs is no longer actively maintained as well as other gtk3-rs crates.

Website: <https://world.pages.gitlab.gnome.org/Rust/libdazzle-rs>

## Documentation

- libdazzle: <https://world.pages.gitlab.gnome.org/Rust/libdazzle-rs/stable/latest/docs/libdazzle>
- libdazzle-sys: <https://world.pages.gitlab.gnome.org/Rust/libdazzle-rs/stable/latest/docs/libdazzle_sys>
