// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{prelude::*, translate::*};
use std::fmt;

glib::wrapper! {
    #[doc(alias = "DzlSettingsSandwich")]
    pub struct SettingsSandwich(Object<ffi::DzlSettingsSandwich, ffi::DzlSettingsSandwichClass>);

    match fn {
        type_ => || ffi::dzl_settings_sandwich_get_type(),
    }
}

impl SettingsSandwich {
    #[doc(alias = "dzl_settings_sandwich_new")]
    pub fn new(schema_id: &str, path: &str) -> SettingsSandwich {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::dzl_settings_sandwich_new(
                schema_id.to_glib_none().0,
                path.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_settings_sandwich_append")]
    pub fn append(&self, settings: &impl IsA<gio::Settings>) {
        unsafe {
            ffi::dzl_settings_sandwich_append(
                self.to_glib_none().0,
                settings.as_ref().to_glib_none().0,
            );
        }
    }

    //#[doc(alias = "dzl_settings_sandwich_bind")]
    //pub fn bind(&self, key: &str, object: /*Unimplemented*/Option<Basic: Pointer>, property: &str, flags: gio::SettingsBindFlags) {
    //    unsafe { TODO: call ffi:dzl_settings_sandwich_bind() }
    //}

    //#[doc(alias = "dzl_settings_sandwich_bind_with_mapping")]
    //pub fn bind_with_mapping<P: Fn(&glib::Value, &glib::Variant) -> bool + 'static, Q: Fn(&glib::Value, &glib::VariantType) -> glib::Variant + 'static>(&self, key: &str, object: /*Unimplemented*/Option<Basic: Pointer>, property: &str, flags: gio::SettingsBindFlags, get_mapping: P, set_mapping: Q) {
    //    unsafe { TODO: call ffi:dzl_settings_sandwich_bind_with_mapping() }
    //}

    #[doc(alias = "dzl_settings_sandwich_get_boolean")]
    #[doc(alias = "get_boolean")]
    pub fn is_boolean(&self, key: &str) -> bool {
        unsafe {
            from_glib(ffi::dzl_settings_sandwich_get_boolean(
                self.to_glib_none().0,
                key.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_settings_sandwich_get_default_value")]
    #[doc(alias = "get_default_value")]
    pub fn default_value(&self, key: &str) -> Option<glib::Variant> {
        unsafe {
            from_glib_full(ffi::dzl_settings_sandwich_get_default_value(
                self.to_glib_none().0,
                key.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_settings_sandwich_get_double")]
    #[doc(alias = "get_double")]
    pub fn double(&self, key: &str) -> f64 {
        unsafe {
            ffi::dzl_settings_sandwich_get_double(self.to_glib_none().0, key.to_glib_none().0)
        }
    }

    #[doc(alias = "dzl_settings_sandwich_get_int")]
    #[doc(alias = "get_int")]
    pub fn int(&self, key: &str) -> i32 {
        unsafe { ffi::dzl_settings_sandwich_get_int(self.to_glib_none().0, key.to_glib_none().0) }
    }

    #[doc(alias = "dzl_settings_sandwich_get_string")]
    #[doc(alias = "get_string")]
    pub fn string(&self, key: &str) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::dzl_settings_sandwich_get_string(
                self.to_glib_none().0,
                key.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_settings_sandwich_get_uint")]
    #[doc(alias = "get_uint")]
    pub fn uint(&self, key: &str) -> u32 {
        unsafe { ffi::dzl_settings_sandwich_get_uint(self.to_glib_none().0, key.to_glib_none().0) }
    }

    #[doc(alias = "dzl_settings_sandwich_get_user_value")]
    #[doc(alias = "get_user_value")]
    pub fn user_value(&self, key: &str) -> Option<glib::Variant> {
        unsafe {
            from_glib_full(ffi::dzl_settings_sandwich_get_user_value(
                self.to_glib_none().0,
                key.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_settings_sandwich_get_value")]
    #[doc(alias = "get_value")]
    pub fn value(&self, key: &str) -> Option<glib::Variant> {
        unsafe {
            from_glib_full(ffi::dzl_settings_sandwich_get_value(
                self.to_glib_none().0,
                key.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_settings_sandwich_set_boolean")]
    pub fn set_boolean(&self, key: &str, val: bool) {
        unsafe {
            ffi::dzl_settings_sandwich_set_boolean(
                self.to_glib_none().0,
                key.to_glib_none().0,
                val.into_glib(),
            );
        }
    }

    #[doc(alias = "dzl_settings_sandwich_set_double")]
    pub fn set_double(&self, key: &str, val: f64) {
        unsafe {
            ffi::dzl_settings_sandwich_set_double(self.to_glib_none().0, key.to_glib_none().0, val);
        }
    }

    #[doc(alias = "dzl_settings_sandwich_set_int")]
    pub fn set_int(&self, key: &str, val: i32) {
        unsafe {
            ffi::dzl_settings_sandwich_set_int(self.to_glib_none().0, key.to_glib_none().0, val);
        }
    }

    #[doc(alias = "dzl_settings_sandwich_set_string")]
    pub fn set_string(&self, key: &str, val: &str) {
        unsafe {
            ffi::dzl_settings_sandwich_set_string(
                self.to_glib_none().0,
                key.to_glib_none().0,
                val.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_settings_sandwich_set_uint")]
    pub fn set_uint(&self, key: &str, val: u32) {
        unsafe {
            ffi::dzl_settings_sandwich_set_uint(self.to_glib_none().0, key.to_glib_none().0, val);
        }
    }

    #[doc(alias = "dzl_settings_sandwich_set_value")]
    pub fn set_value(&self, key: &str, value: &glib::Variant) {
        unsafe {
            ffi::dzl_settings_sandwich_set_value(
                self.to_glib_none().0,
                key.to_glib_none().0,
                value.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_settings_sandwich_unbind")]
    pub fn unbind(&self, property: &str) {
        unsafe {
            ffi::dzl_settings_sandwich_unbind(self.to_glib_none().0, property.to_glib_none().0);
        }
    }

    pub fn path(&self) -> Option<glib::GString> {
        ObjectExt::property(self, "path")
    }

    #[doc(alias = "schema-id")]
    pub fn schema_id(&self) -> Option<glib::GString> {
        ObjectExt::property(self, "schema-id")
    }
}

impl fmt::Display for SettingsSandwich {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("SettingsSandwich")
    }
}
