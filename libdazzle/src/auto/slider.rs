// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::SliderPosition;
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlSlider")]
    pub struct Slider(Object<ffi::DzlSlider, ffi::DzlSliderClass>) @extends gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        type_ => || ffi::dzl_slider_get_type(),
    }
}

impl Slider {
    pub const NONE: Option<&'static Slider> = None;

    #[doc(alias = "dzl_slider_new")]
    pub fn new() -> Slider {
        assert_initialized_main_thread!();
        unsafe { gtk::Widget::from_glib_none(ffi::dzl_slider_new()).unsafe_cast() }
    }
}

impl Default for Slider {
    fn default() -> Self {
        Self::new()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Slider>> Sealed for T {}
}

pub trait SliderExt: IsA<Slider> + sealed::Sealed + 'static {
    #[doc(alias = "dzl_slider_add_slider")]
    fn add_slider(&self, widget: &impl IsA<gtk::Widget>, position: SliderPosition) {
        unsafe {
            ffi::dzl_slider_add_slider(
                self.as_ref().to_glib_none().0,
                widget.as_ref().to_glib_none().0,
                position.into_glib(),
            );
        }
    }

    #[doc(alias = "dzl_slider_get_position")]
    #[doc(alias = "get_position")]
    fn position(&self) -> SliderPosition {
        unsafe { from_glib(ffi::dzl_slider_get_position(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "dzl_slider_set_position")]
    fn set_position(&self, position: SliderPosition) {
        unsafe {
            ffi::dzl_slider_set_position(self.as_ref().to_glib_none().0, position.into_glib());
        }
    }

    #[doc(alias = "position")]
    fn connect_position_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_position_trampoline<P: IsA<Slider>, F: Fn(&P) + 'static>(
            this: *mut ffi::DzlSlider,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(Slider::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::position\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_position_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<Slider>> SliderExt for O {}

impl fmt::Display for Slider {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Slider")
    }
}
