// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute, pin::Pin, ptr};

glib::wrapper! {
    #[doc(alias = "DzlFuzzyIndexBuilder")]
    pub struct FuzzyIndexBuilder(Object<ffi::DzlFuzzyIndexBuilder, ffi::DzlFuzzyIndexBuilderClass>);

    match fn {
        type_ => || ffi::dzl_fuzzy_index_builder_get_type(),
    }
}

impl FuzzyIndexBuilder {
    #[doc(alias = "dzl_fuzzy_index_builder_new")]
    pub fn new() -> FuzzyIndexBuilder {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::dzl_fuzzy_index_builder_new()) }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_get_case_sensitive")]
    #[doc(alias = "get_case_sensitive")]
    pub fn is_case_sensitive(&self) -> bool {
        unsafe {
            from_glib(ffi::dzl_fuzzy_index_builder_get_case_sensitive(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_get_document")]
    #[doc(alias = "get_document")]
    pub fn document(&self, document_id: u64) -> Option<glib::Variant> {
        unsafe {
            from_glib_none(ffi::dzl_fuzzy_index_builder_get_document(
                self.to_glib_none().0,
                document_id,
            ))
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_insert")]
    pub fn insert(&self, key: &str, document: &glib::Variant, priority: u32) -> u64 {
        unsafe {
            ffi::dzl_fuzzy_index_builder_insert(
                self.to_glib_none().0,
                key.to_glib_none().0,
                document.to_glib_none().0,
                priority,
            )
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_set_case_sensitive")]
    pub fn set_case_sensitive(&self, case_sensitive: bool) {
        unsafe {
            ffi::dzl_fuzzy_index_builder_set_case_sensitive(
                self.to_glib_none().0,
                case_sensitive.into_glib(),
            );
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_set_metadata")]
    pub fn set_metadata(&self, key: &str, value: &glib::Variant) {
        unsafe {
            ffi::dzl_fuzzy_index_builder_set_metadata(
                self.to_glib_none().0,
                key.to_glib_none().0,
                value.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_set_metadata_string")]
    pub fn set_metadata_string(&self, key: &str, value: &str) {
        unsafe {
            ffi::dzl_fuzzy_index_builder_set_metadata_string(
                self.to_glib_none().0,
                key.to_glib_none().0,
                value.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_set_metadata_uint32")]
    pub fn set_metadata_uint32(&self, key: &str, value: u32) {
        unsafe {
            ffi::dzl_fuzzy_index_builder_set_metadata_uint32(
                self.to_glib_none().0,
                key.to_glib_none().0,
                value,
            );
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_set_metadata_uint64")]
    pub fn set_metadata_uint64(&self, key: &str, value: u64) {
        unsafe {
            ffi::dzl_fuzzy_index_builder_set_metadata_uint64(
                self.to_glib_none().0,
                key.to_glib_none().0,
                value,
            );
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_write")]
    pub fn write(
        &self,
        file: &impl IsA<gio::File>,
        io_priority: i32,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::dzl_fuzzy_index_builder_write(
                self.to_glib_none().0,
                file.as_ref().to_glib_none().0,
                io_priority,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "dzl_fuzzy_index_builder_write_async")]
    pub fn write_async<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        file: &impl IsA<gio::File>,
        io_priority: glib::Priority,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn write_async_trampoline<
            P: FnOnce(Result<(), glib::Error>) + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = ptr::null_mut();
            let _ = ffi::dzl_fuzzy_index_builder_write_finish(
                _source_object as *mut _,
                res,
                &mut error,
            );
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = write_async_trampoline::<P>;
        unsafe {
            ffi::dzl_fuzzy_index_builder_write_async(
                self.to_glib_none().0,
                file.as_ref().to_glib_none().0,
                io_priority.into_glib(),
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    pub fn write_future(
        &self,
        file: &(impl IsA<gio::File> + Clone + 'static),
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let file = file.clone();
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.write_async(&file, io_priority, Some(cancellable), move |res| {
                send.resolve(res);
            });
        }))
    }

    #[doc(alias = "case-sensitive")]
    pub fn connect_case_sensitive_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_case_sensitive_trampoline<
            F: Fn(&FuzzyIndexBuilder) + 'static,
        >(
            this: *mut ffi::DzlFuzzyIndexBuilder,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::case-sensitive\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_case_sensitive_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl Default for FuzzyIndexBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl fmt::Display for FuzzyIndexBuilder {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("FuzzyIndexBuilder")
    }
}
