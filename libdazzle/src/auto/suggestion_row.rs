// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ListBoxRow, Suggestion};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

#[cfg(feature = "gtk_v3_4")]
#[cfg_attr(docsrs, doc(cfg(feature = "gtk_v3_4")))]
glib::wrapper! {
    #[doc(alias = "DzlSuggestionRow")]
    pub struct SuggestionRow(Object<ffi::DzlSuggestionRow, ffi::DzlSuggestionRowClass>) @extends ListBoxRow, gtk::ListBoxRow, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable, gtk::Actionable, gtk::Orientable;

    match fn {
        type_ => || ffi::dzl_suggestion_row_get_type(),
    }
}

#[cfg(not(any(feature = "gtk_v3_4")))]
glib::wrapper! {
    #[doc(alias = "DzlSuggestionRow")]
    pub struct SuggestionRow(Object<ffi::DzlSuggestionRow, ffi::DzlSuggestionRowClass>) @extends ListBoxRow, gtk::ListBoxRow, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable, gtk::Orientable;

    match fn {
        type_ => || ffi::dzl_suggestion_row_get_type(),
    }
}

impl SuggestionRow {
    pub const NONE: Option<&'static SuggestionRow> = None;

    #[doc(alias = "dzl_suggestion_row_new")]
    pub fn new() -> SuggestionRow {
        assert_initialized_main_thread!();
        unsafe { gtk::Widget::from_glib_none(ffi::dzl_suggestion_row_new()).unsafe_cast() }
    }
}

impl Default for SuggestionRow {
    fn default() -> Self {
        Self::new()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::SuggestionRow>> Sealed for T {}
}

pub trait SuggestionRowExt: IsA<SuggestionRow> + sealed::Sealed + 'static {
    #[doc(alias = "dzl_suggestion_row_get_suggestion")]
    #[doc(alias = "get_suggestion")]
    fn suggestion(&self) -> Option<Suggestion> {
        unsafe {
            from_glib_none(ffi::dzl_suggestion_row_get_suggestion(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_suggestion_row_set_suggestion")]
    fn set_suggestion(&self, suggestion: &impl IsA<Suggestion>) {
        unsafe {
            ffi::dzl_suggestion_row_set_suggestion(
                self.as_ref().to_glib_none().0,
                suggestion.as_ref().to_glib_none().0,
            );
        }
    }

    #[doc(alias = "suggestion")]
    fn connect_suggestion_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_suggestion_trampoline<
            P: IsA<SuggestionRow>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlSuggestionRow,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(SuggestionRow::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::suggestion\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_suggestion_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<SuggestionRow>> SuggestionRowExt for O {}

impl fmt::Display for SuggestionRow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("SuggestionRow")
    }
}
