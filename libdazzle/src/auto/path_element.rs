// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

#[cfg(feature = "v3_26")]
#[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
use glib::translate::*;
use std::fmt;

glib::wrapper! {
    #[doc(alias = "DzlPathElement")]
    pub struct PathElement(Object<ffi::DzlPathElement, ffi::DzlPathElementClass>);

    match fn {
        type_ => || ffi::dzl_path_element_get_type(),
    }
}

impl PathElement {
    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_path_element_new")]
    pub fn new(id: Option<&str>, icon_name: Option<&str>, title: &str) -> PathElement {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::dzl_path_element_new(
                id.to_glib_none().0,
                icon_name.to_glib_none().0,
                title.to_glib_none().0,
            ))
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_path_element_get_icon_name")]
    #[doc(alias = "get_icon_name")]
    pub fn icon_name(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::dzl_path_element_get_icon_name(self.to_glib_none().0)) }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_path_element_get_id")]
    #[doc(alias = "get_id")]
    pub fn id(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::dzl_path_element_get_id(self.to_glib_none().0)) }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_path_element_get_title")]
    #[doc(alias = "get_title")]
    pub fn title(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::dzl_path_element_get_title(self.to_glib_none().0)) }
    }
}

impl fmt::Display for PathElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("PathElement")
    }
}
