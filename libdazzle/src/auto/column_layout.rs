// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlColumnLayout")]
    pub struct ColumnLayout(Object<ffi::DzlColumnLayout, ffi::DzlColumnLayoutClass>) @extends gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        type_ => || ffi::dzl_column_layout_get_type(),
    }
}

impl ColumnLayout {
    pub const NONE: Option<&'static ColumnLayout> = None;

    #[doc(alias = "dzl_column_layout_new")]
    pub fn new() -> ColumnLayout {
        assert_initialized_main_thread!();
        unsafe { gtk::Widget::from_glib_none(ffi::dzl_column_layout_new()).unsafe_cast() }
    }
}

impl Default for ColumnLayout {
    fn default() -> Self {
        Self::new()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::ColumnLayout>> Sealed for T {}
}

pub trait ColumnLayoutExt: IsA<ColumnLayout> + sealed::Sealed + 'static {
    #[doc(alias = "dzl_column_layout_get_column_spacing")]
    #[doc(alias = "get_column_spacing")]
    fn column_spacing(&self) -> i32 {
        unsafe { ffi::dzl_column_layout_get_column_spacing(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "dzl_column_layout_get_column_width")]
    #[doc(alias = "get_column_width")]
    fn column_width(&self) -> i32 {
        unsafe { ffi::dzl_column_layout_get_column_width(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "dzl_column_layout_get_max_columns")]
    #[doc(alias = "get_max_columns")]
    fn max_columns(&self) -> u32 {
        unsafe { ffi::dzl_column_layout_get_max_columns(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "dzl_column_layout_get_row_spacing")]
    #[doc(alias = "get_row_spacing")]
    fn row_spacing(&self) -> i32 {
        unsafe { ffi::dzl_column_layout_get_row_spacing(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "dzl_column_layout_set_column_spacing")]
    fn set_column_spacing(&self, column_spacing: i32) {
        unsafe {
            ffi::dzl_column_layout_set_column_spacing(
                self.as_ref().to_glib_none().0,
                column_spacing,
            );
        }
    }

    #[doc(alias = "dzl_column_layout_set_column_width")]
    fn set_column_width(&self, column_width: i32) {
        unsafe {
            ffi::dzl_column_layout_set_column_width(self.as_ref().to_glib_none().0, column_width);
        }
    }

    #[doc(alias = "dzl_column_layout_set_max_columns")]
    fn set_max_columns(&self, max_columns: u32) {
        unsafe {
            ffi::dzl_column_layout_set_max_columns(self.as_ref().to_glib_none().0, max_columns);
        }
    }

    #[doc(alias = "dzl_column_layout_set_row_spacing")]
    fn set_row_spacing(&self, row_spacing: i32) {
        unsafe {
            ffi::dzl_column_layout_set_row_spacing(self.as_ref().to_glib_none().0, row_spacing);
        }
    }

    #[doc(alias = "column-spacing")]
    fn connect_column_spacing_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_column_spacing_trampoline<
            P: IsA<ColumnLayout>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlColumnLayout,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(ColumnLayout::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::column-spacing\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_column_spacing_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "column-width")]
    fn connect_column_width_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_column_width_trampoline<
            P: IsA<ColumnLayout>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlColumnLayout,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(ColumnLayout::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::column-width\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_column_width_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "max-columns")]
    fn connect_max_columns_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_max_columns_trampoline<
            P: IsA<ColumnLayout>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlColumnLayout,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(ColumnLayout::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::max-columns\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_max_columns_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "row-spacing")]
    fn connect_row_spacing_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_row_spacing_trampoline<
            P: IsA<ColumnLayout>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlColumnLayout,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(ColumnLayout::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::row-spacing\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_row_spacing_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<ColumnLayout>> ColumnLayoutExt for O {}

impl fmt::Display for ColumnLayout {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("ColumnLayout")
    }
}
