// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{prelude::*, translate::*};
use std::fmt;

glib::wrapper! {
    #[doc(alias = "DzlWidgetActionGroup")]
    pub struct WidgetActionGroup(Object<ffi::DzlWidgetActionGroup, ffi::DzlWidgetActionGroupClass>) @implements gio::ActionGroup;

    match fn {
        type_ => || ffi::dzl_widget_action_group_get_type(),
    }
}

impl WidgetActionGroup {
    #[doc(alias = "dzl_widget_action_group_set_action_enabled")]
    pub fn set_action_enabled(&self, action_name: &str, enabled: bool) {
        unsafe {
            ffi::dzl_widget_action_group_set_action_enabled(
                self.to_glib_none().0,
                action_name.to_glib_none().0,
                enabled.into_glib(),
            );
        }
    }

    pub fn widget(&self) -> Option<gtk::Widget> {
        ObjectExt::property(self, "widget")
    }

    #[doc(alias = "dzl_widget_action_group_attach")]
    pub fn attach(widget: &impl IsA<gtk::Widget>, group_name: &str) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::dzl_widget_action_group_attach(
                widget.as_ref().to_glib_none().0,
                group_name.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_widget_action_group_new")]
    pub fn new(widget: &impl IsA<gtk::Widget>) -> Option<gio::ActionGroup> {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::dzl_widget_action_group_new(
                widget.as_ref().to_glib_none().0,
            ))
        }
    }
}

impl fmt::Display for WidgetActionGroup {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("WidgetActionGroup")
    }
}
