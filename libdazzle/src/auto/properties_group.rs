// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

#[cfg(feature = "v3_26")]
#[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
use crate::PropertiesFlags;
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlPropertiesGroup")]
    pub struct PropertiesGroup(Object<ffi::DzlPropertiesGroup, ffi::DzlPropertiesGroupClass>) @implements gio::ActionGroup;

    match fn {
        type_ => || ffi::dzl_properties_group_get_type(),
    }
}

impl PropertiesGroup {
    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_properties_group_new")]
    pub fn new(object: &impl IsA<glib::Object>) -> PropertiesGroup {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::dzl_properties_group_new(
                object.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_properties_group_new_for_type")]
    #[doc(alias = "new_for_type")]
    pub fn for_type(object_type: glib::types::Type) -> PropertiesGroup {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_none(ffi::dzl_properties_group_new_for_type(
                object_type.into_glib(),
            ))
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_properties_group_add_all_properties")]
    pub fn add_all_properties(&self) {
        unsafe {
            ffi::dzl_properties_group_add_all_properties(self.to_glib_none().0);
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_properties_group_add_property")]
    pub fn add_property(&self, name: &str, property_name: &str) {
        unsafe {
            ffi::dzl_properties_group_add_property(
                self.to_glib_none().0,
                name.to_glib_none().0,
                property_name.to_glib_none().0,
            );
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_properties_group_add_property_full")]
    pub fn add_property_full(&self, name: &str, property_name: &str, flags: PropertiesFlags) {
        unsafe {
            ffi::dzl_properties_group_add_property_full(
                self.to_glib_none().0,
                name.to_glib_none().0,
                property_name.to_glib_none().0,
                flags.into_glib(),
            );
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_properties_group_remove")]
    pub fn remove(&self, name: &str) {
        unsafe {
            ffi::dzl_properties_group_remove(self.to_glib_none().0, name.to_glib_none().0);
        }
    }

    pub fn object(&self) -> Option<glib::Object> {
        ObjectExt::property(self, "object")
    }

    pub fn set_object<P: IsA<glib::Object>>(&self, object: Option<&P>) {
        ObjectExt::set_property(self, "object", object)
    }

    #[doc(alias = "object-type")]
    pub fn object_type(&self) -> glib::types::Type {
        ObjectExt::property(self, "object-type")
    }

    #[doc(alias = "object")]
    pub fn connect_object_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_object_trampoline<F: Fn(&PropertiesGroup) + 'static>(
            this: *mut ffi::DzlPropertiesGroup,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::object\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_object_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl fmt::Display for PropertiesGroup {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("PropertiesGroup")
    }
}
