// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlApplicationWindow")]
    pub struct ApplicationWindow(Object<ffi::DzlApplicationWindow, ffi::DzlApplicationWindowClass>) @extends gtk::ApplicationWindow, gtk::Window, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable, gio::ActionGroup;

    match fn {
        type_ => || ffi::dzl_application_window_get_type(),
    }
}

impl ApplicationWindow {
    pub const NONE: Option<&'static ApplicationWindow> = None;
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::ApplicationWindow>> Sealed for T {}
}

pub trait ApplicationWindowExt: IsA<ApplicationWindow> + sealed::Sealed + 'static {
    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_application_window_get_fullscreen")]
    #[doc(alias = "get_fullscreen")]
    fn is_fullscreen(&self) -> bool {
        unsafe {
            from_glib(ffi::dzl_application_window_get_fullscreen(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_application_window_get_titlebar")]
    #[doc(alias = "get_titlebar")]
    fn titlebar(&self) -> Option<gtk::Widget> {
        unsafe {
            from_glib_none(ffi::dzl_application_window_get_titlebar(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    //#[doc(alias = "dzl_application_window_get_titlebar_animation")]
    //#[doc(alias = "get_titlebar_animation")]
    //fn titlebar_animation(&self) -> /*Ignored*/TitlebarAnimation {
    //    unsafe { TODO: call ffi:dzl_application_window_get_titlebar_animation() }
    //}

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_application_window_set_fullscreen")]
    fn set_fullscreen(&self, fullscreen: bool) {
        unsafe {
            ffi::dzl_application_window_set_fullscreen(
                self.as_ref().to_glib_none().0,
                fullscreen.into_glib(),
            );
        }
    }

    #[cfg(feature = "v3_26")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v3_26")))]
    #[doc(alias = "dzl_application_window_set_titlebar")]
    fn set_titlebar(&self, titlebar: &impl IsA<gtk::Widget>) {
        unsafe {
            ffi::dzl_application_window_set_titlebar(
                self.as_ref().to_glib_none().0,
                titlebar.as_ref().to_glib_none().0,
            );
        }
    }

    fn get_property_fullscreen(&self) -> bool {
        ObjectExt::property(self.as_ref(), "fullscreen")
    }

    fn set_property_fullscreen(&self, fullscreen: bool) {
        ObjectExt::set_property(self.as_ref(), "fullscreen", fullscreen)
    }

    #[doc(alias = "fullscreen")]
    fn connect_fullscreen_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_fullscreen_trampoline<
            P: IsA<ApplicationWindow>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlApplicationWindow,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(ApplicationWindow::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::fullscreen\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_fullscreen_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "titlebar-animation")]
    fn connect_titlebar_animation_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_titlebar_animation_trampoline<
            P: IsA<ApplicationWindow>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::DzlApplicationWindow,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(ApplicationWindow::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::titlebar-animation\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_titlebar_animation_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<ApplicationWindow>> ApplicationWindowExt for O {}

impl fmt::Display for ApplicationWindow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("ApplicationWindow")
    }
}
