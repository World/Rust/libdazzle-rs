// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlListModelFilter")]
    pub struct ListModelFilter(Object<ffi::DzlListModelFilter, ffi::DzlListModelFilterClass>) @implements gio::ListModel;

    match fn {
        type_ => || ffi::dzl_list_model_filter_get_type(),
    }
}

impl ListModelFilter {
    #[doc(alias = "dzl_list_model_filter_new")]
    pub fn new(child_model: &impl IsA<gio::ListModel>) -> ListModelFilter {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::dzl_list_model_filter_new(
                child_model.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_list_model_filter_get_child_model")]
    #[doc(alias = "get_child_model")]
    pub fn child_model(&self) -> Option<gio::ListModel> {
        unsafe {
            from_glib_none(ffi::dzl_list_model_filter_get_child_model(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_list_model_filter_invalidate")]
    pub fn invalidate(&self) {
        unsafe {
            ffi::dzl_list_model_filter_invalidate(self.to_glib_none().0);
        }
    }

    #[doc(alias = "dzl_list_model_filter_set_filter_func")]
    pub fn set_filter_func<P: Fn(&glib::Object) -> bool + 'static>(&self, filter_func: P) {
        let filter_func_data: Box_<P> = Box_::new(filter_func);
        unsafe extern "C" fn filter_func_func<P: Fn(&glib::Object) -> bool + 'static>(
            object: *mut glib::gobject_ffi::GObject,
            user_data: glib::ffi::gpointer,
        ) -> glib::ffi::gboolean {
            let object = from_glib_borrow(object);
            let callback: &P = &*(user_data as *mut _);
            (*callback)(&object).into_glib()
        }
        let filter_func = Some(filter_func_func::<P> as _);
        unsafe extern "C" fn filter_func_data_destroy_func<
            P: Fn(&glib::Object) -> bool + 'static,
        >(
            data: glib::ffi::gpointer,
        ) {
            let _callback: Box_<P> = Box_::from_raw(data as *mut _);
        }
        let destroy_call3 = Some(filter_func_data_destroy_func::<P> as _);
        let super_callback0: Box_<P> = filter_func_data;
        unsafe {
            ffi::dzl_list_model_filter_set_filter_func(
                self.to_glib_none().0,
                filter_func,
                Box_::into_raw(super_callback0) as *mut _,
                destroy_call3,
            );
        }
    }

    #[doc(alias = "child-model")]
    pub fn connect_child_model_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_child_model_trampoline<F: Fn(&ListModelFilter) + 'static>(
            this: *mut ffi::DzlListModelFilter,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::child-model\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_child_model_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl fmt::Display for ListModelFilter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("ListModelFilter")
    }
}
