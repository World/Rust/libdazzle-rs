// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::translate::*;
use std::fmt;

glib::wrapper! {
    #[doc(alias = "DzlFuzzyIndexMatch")]
    pub struct FuzzyIndexMatch(Object<ffi::DzlFuzzyIndexMatch, ffi::DzlFuzzyIndexMatchClass>);

    match fn {
        type_ => || ffi::dzl_fuzzy_index_match_get_type(),
    }
}

impl FuzzyIndexMatch {
    #[doc(alias = "dzl_fuzzy_index_match_get_document")]
    #[doc(alias = "get_document")]
    pub fn document(&self) -> Option<glib::Variant> {
        unsafe {
            from_glib_none(ffi::dzl_fuzzy_index_match_get_document(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_fuzzy_index_match_get_key")]
    #[doc(alias = "get_key")]
    pub fn key(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::dzl_fuzzy_index_match_get_key(self.to_glib_none().0)) }
    }

    #[doc(alias = "dzl_fuzzy_index_match_get_priority")]
    #[doc(alias = "get_priority")]
    pub fn priority(&self) -> u32 {
        unsafe { ffi::dzl_fuzzy_index_match_get_priority(self.to_glib_none().0) }
    }

    #[doc(alias = "dzl_fuzzy_index_match_get_score")]
    #[doc(alias = "get_score")]
    pub fn score(&self) -> f32 {
        unsafe { ffi::dzl_fuzzy_index_match_get_score(self.to_glib_none().0) }
    }
}

impl fmt::Display for FuzzyIndexMatch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("FuzzyIndexMatch")
    }
}
