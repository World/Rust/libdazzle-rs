// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlPillBox")]
    pub struct PillBox(Object<ffi::DzlPillBox, ffi::DzlPillBoxClass>) @extends gtk::EventBox, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        type_ => || ffi::dzl_pill_box_get_type(),
    }
}

impl PillBox {
    #[doc(alias = "dzl_pill_box_new")]
    pub fn new(label: &str) -> PillBox {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(ffi::dzl_pill_box_new(label.to_glib_none().0)).unsafe_cast()
        }
    }

    #[doc(alias = "dzl_pill_box_get_label")]
    #[doc(alias = "get_label")]
    pub fn label(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::dzl_pill_box_get_label(self.to_glib_none().0)) }
    }

    #[doc(alias = "dzl_pill_box_set_label")]
    pub fn set_label(&self, label: &str) {
        unsafe {
            ffi::dzl_pill_box_set_label(self.to_glib_none().0, label.to_glib_none().0);
        }
    }

    #[doc(alias = "label")]
    pub fn connect_label_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_label_trampoline<F: Fn(&PillBox) + 'static>(
            this: *mut ffi::DzlPillBox,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::label\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_label_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl fmt::Display for PillBox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("PillBox")
    }
}
