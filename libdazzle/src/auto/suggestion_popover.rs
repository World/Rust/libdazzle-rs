// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::Suggestion;
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "DzlSuggestionPopover")]
    pub struct SuggestionPopover(Object<ffi::DzlSuggestionPopover, ffi::DzlSuggestionPopoverClass>) @extends gtk::Window, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        type_ => || ffi::dzl_suggestion_popover_get_type(),
    }
}

impl SuggestionPopover {
    #[doc(alias = "dzl_suggestion_popover_new")]
    pub fn new() -> SuggestionPopover {
        assert_initialized_main_thread!();
        unsafe { gtk::Widget::from_glib_none(ffi::dzl_suggestion_popover_new()).unsafe_cast() }
    }

    #[doc(alias = "dzl_suggestion_popover_activate_selected")]
    pub fn activate_selected(&self) {
        unsafe {
            ffi::dzl_suggestion_popover_activate_selected(self.to_glib_none().0);
        }
    }

    #[doc(alias = "dzl_suggestion_popover_get_model")]
    #[doc(alias = "get_model")]
    pub fn model(&self) -> Option<gio::ListModel> {
        unsafe { from_glib_none(ffi::dzl_suggestion_popover_get_model(self.to_glib_none().0)) }
    }

    #[doc(alias = "dzl_suggestion_popover_get_relative_to")]
    #[doc(alias = "get_relative_to")]
    pub fn relative_to(&self) -> Option<gtk::Widget> {
        unsafe {
            from_glib_none(ffi::dzl_suggestion_popover_get_relative_to(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_suggestion_popover_get_selected")]
    #[doc(alias = "get_selected")]
    pub fn selected(&self) -> Option<Suggestion> {
        unsafe {
            from_glib_none(ffi::dzl_suggestion_popover_get_selected(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "dzl_suggestion_popover_move_by")]
    pub fn move_by(&self, amount: i32) {
        unsafe {
            ffi::dzl_suggestion_popover_move_by(self.to_glib_none().0, amount);
        }
    }

    #[doc(alias = "dzl_suggestion_popover_popdown")]
    pub fn popdown(&self) {
        unsafe {
            ffi::dzl_suggestion_popover_popdown(self.to_glib_none().0);
        }
    }

    #[doc(alias = "dzl_suggestion_popover_popup")]
    pub fn popup(&self) {
        unsafe {
            ffi::dzl_suggestion_popover_popup(self.to_glib_none().0);
        }
    }

    #[doc(alias = "dzl_suggestion_popover_set_model")]
    pub fn set_model(&self, model: &impl IsA<gio::ListModel>) {
        unsafe {
            ffi::dzl_suggestion_popover_set_model(
                self.to_glib_none().0,
                model.as_ref().to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_suggestion_popover_set_relative_to")]
    pub fn set_relative_to(&self, widget: &impl IsA<gtk::Widget>) {
        unsafe {
            ffi::dzl_suggestion_popover_set_relative_to(
                self.to_glib_none().0,
                widget.as_ref().to_glib_none().0,
            );
        }
    }

    #[doc(alias = "dzl_suggestion_popover_set_selected")]
    pub fn set_selected(&self, suggestion: &impl IsA<Suggestion>) {
        unsafe {
            ffi::dzl_suggestion_popover_set_selected(
                self.to_glib_none().0,
                suggestion.as_ref().to_glib_none().0,
            );
        }
    }

    #[doc(alias = "subtitle-ellipsize")]
    pub fn subtitle_ellipsize(&self) -> pango::EllipsizeMode {
        ObjectExt::property(self, "subtitle-ellipsize")
    }

    #[doc(alias = "subtitle-ellipsize")]
    pub fn set_subtitle_ellipsize(&self, subtitle_ellipsize: pango::EllipsizeMode) {
        ObjectExt::set_property(self, "subtitle-ellipsize", subtitle_ellipsize)
    }

    #[doc(alias = "title-ellipsize")]
    pub fn title_ellipsize(&self) -> pango::EllipsizeMode {
        ObjectExt::property(self, "title-ellipsize")
    }

    #[doc(alias = "title-ellipsize")]
    pub fn set_title_ellipsize(&self, title_ellipsize: pango::EllipsizeMode) {
        ObjectExt::set_property(self, "title-ellipsize", title_ellipsize)
    }

    #[doc(alias = "suggestion-activated")]
    pub fn connect_suggestion_activated<F: Fn(&Self, &Suggestion) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn suggestion_activated_trampoline<
            F: Fn(&SuggestionPopover, &Suggestion) + 'static,
        >(
            this: *mut ffi::DzlSuggestionPopover,
            object: *mut ffi::DzlSuggestion,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this), &from_glib_borrow(object))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"suggestion-activated\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    suggestion_activated_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "model")]
    pub fn connect_model_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_model_trampoline<F: Fn(&SuggestionPopover) + 'static>(
            this: *mut ffi::DzlSuggestionPopover,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::model\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_model_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "relative-to")]
    pub fn connect_relative_to_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_relative_to_trampoline<F: Fn(&SuggestionPopover) + 'static>(
            this: *mut ffi::DzlSuggestionPopover,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::relative-to\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_relative_to_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "selected")]
    pub fn connect_selected_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_selected_trampoline<F: Fn(&SuggestionPopover) + 'static>(
            this: *mut ffi::DzlSuggestionPopover,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::selected\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_selected_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "subtitle-ellipsize")]
    pub fn connect_subtitle_ellipsize_notify<F: Fn(&Self) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_subtitle_ellipsize_trampoline<
            F: Fn(&SuggestionPopover) + 'static,
        >(
            this: *mut ffi::DzlSuggestionPopover,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::subtitle-ellipsize\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_subtitle_ellipsize_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "title-ellipsize")]
    pub fn connect_title_ellipsize_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_title_ellipsize_trampoline<
            F: Fn(&SuggestionPopover) + 'static,
        >(
            this: *mut ffi::DzlSuggestionPopover,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::title-ellipsize\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_title_ellipsize_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl Default for SuggestionPopover {
    fn default() -> Self {
        Self::new()
    }
}

impl fmt::Display for SuggestionPopover {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("SuggestionPopover")
    }
}
