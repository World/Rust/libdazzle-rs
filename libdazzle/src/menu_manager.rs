use crate::MenuManager;
use glib::translate::*;
use std::ptr;

impl MenuManager {
    #[doc(alias = "dzl_menu_manager_add_filename")]
    pub fn add_filename(&self, filename: &str) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::dzl_menu_manager_add_filename(
                self.to_glib_none().0,
                filename.to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == 0, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "dzl_menu_manager_add_resource")]
    pub fn add_resource(&self, resource: &str) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::dzl_menu_manager_add_resource(
                self.to_glib_none().0,
                resource.to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == 0, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }
}
